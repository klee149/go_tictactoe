// https://godoc.org/github.com/hajimehoshi/ebiten

package main

import (
	"fmt"
	"image/color"
	"log"
	"strconv"

	"./ai"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

/**
 * getPieceCoords
 * input:	boardX, boardY
 * return:	imgX, imgY
 *
 * description:
 * converts coordinates in array to pixel coordinates
 */
func getPieceCoords(boardX, boardY int, forward bool) (imgX, imgY float64) {
	offsetX := boardWidth / 3
	offsetY := boardHeight / 3

	imgX = float64(boardX*offsetX + offsetX/10)
	imgY = float64(boardY*offsetY + offsetY/10)

	if !forward {
		imgX *= -1
		imgY *= -1
	}
	return
}

/**
 * getMousePosition
 * input:	none
 * return: 	mx, my
 *
 * description:
 * gets the pixel coordinates of mouse and returns which square the mouse is in
 */
func getMousePosition() (mx, my int) {
	cx, cy := ebiten.CursorPosition()
	if cx < line1 {
		mx = 0
	} else if cx >= line1 && cx < line2 {
		mx = 1
	} else {
		mx = 2
	}

	if cy < line1 {
		my = 0
	} else if cy >= line1 && cy < line2 {
		my = 1
	} else {
		my = 2
	}

	return
}

// TODO: get graphical way of choosing between AI vs player and player vs player

/**
 * this is the main game loop
 */
func update(screen *ebiten.Image) error {
	// ebitenutil.DebugPrint(screen, "Hello world!")

	/* Draw board */
	screen.DrawImage(boardImg, nil)

	/* init screen */
	// text.Draw(screen, "hello there", nil, boardWidth/2, boardHeight/2, color.Black)

	/* get mouse location and mark location */
	mx, my := getMousePosition()

	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) && board[mx][my] == 0 && !gameSet {
		// fmt.Println(mx, my)

		pieceImgOps := &ebiten.DrawImageOptions{}
		pieceImgOps.GeoM.Scale(0.1, 0.1)
		pieceImgOps.GeoM.Translate(getPieceCoords(mx, my, true))

		if xTurn {
			board[mx][my] = 1
			boardImg.DrawImage(xImg, pieceImgOps)
			if ai.CheckForWin(mx, my, 1, board) {
				gameSet = true
				fmt.Println("X wins")
			}

		} else {
			board[mx][my] = 2
			boardImg.DrawImage(oImg, pieceImgOps)
			if ai.CheckForWin(mx, my, 2, board) {
				gameSet = true
				fmt.Println("O wins")
			}
		}

		// note: this places the coords back at 0,0
		pieceImgOps.GeoM.Translate(getPieceCoords(mx, my, false))
		xTurn = !xTurn

	}

	/* Draw pieces*/
	// pieceImgOps := &ebiten.DrawImageOptions{}
	// pieceImgOps.GeoM.Scale(0.1, 0.1)

	/* this is probably really inefficient so will replace this with drawing the x or o once */
	// for j := 0; j < len(board); j++ {
	// 	for i := 0; i < len(board[0]); i++ {
	// 		pieceImgOps.GeoM.Translate(getPieceCoords(i, j, true))

	// 		switch board[j][i] {
	// 		case 1:
	// 			screen.DrawImage(xImg, pieceImgOps)
	// 		case 2:
	// 			screen.DrawImage(oImg, pieceImgOps)
	// 		}
	// 		pieceImgOps.GeoM.Translate(getPieceCoords(i, j, false))
	// 	}
	// }

	return nil
}

func selectGameMode() {
	var usrSelection string

	for gameMode < 1 || gameMode > 3 {
		fmt.Println("Select Game Mode: ")
		fmt.Println("1: Player vs Player\n2: Player vs AI\n3: AI vs Player")
		fmt.Scanln(&usrSelection)
		gameMode, _ = strconv.Atoi(usrSelection)
	}
}

var (
	board       = [][]int{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}
	boardImg    *ebiten.Image
	xImg        *ebiten.Image
	oImg        *ebiten.Image
	boardWidth  int
	boardHeight int
	xTurn       = true
	line1       int
	line2       int
	textColor   color.RGBA
	gameMode    = 0
	gameSet     = false
)

func main() {

	ai.TestPrint()
	selectGameMode()

	/* load images */
	var err error
	xImg, _, err = ebitenutil.NewImageFromFile("x.png", ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}

	oImg, _, err = ebitenutil.NewImageFromFile("o.png", ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}

	boardImg, _, err = ebitenutil.NewImageFromFile("board.gif", ebiten.FilterNearest)
	if err != nil {
		log.Fatal(err)
	}

	/* init some size values */
	boardWidth, boardHeight = boardImg.Size()
	line1 = boardWidth / 3
	line2 = line1 * 2

	textColor.A = 0xff
	textColor.R = 0x55
	textColor.G = 0x55
	textColor.B = 0x55

	for j := 0; j < len(board); j++ {
		for i := 0; i < len(board[j]); i++ {
			fmt.Printf("%v ", board[j][i])
		}
		fmt.Println()
	}

	ebiten.Run(update, boardWidth, boardHeight, 3, "Hello world!")
}
