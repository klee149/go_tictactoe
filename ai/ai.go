package ai

import (
	"fmt"
)

func TestPrint() {
	fmt.Println("test print")
}

/**
 * CheckForWin
 * input:	move of most recent player
 *			most recent player ( 1 or 2 )
 * return:	did player win?
 *
 * description:
 * a win can only occur for the player that just made the move and the winning line
 * has to include the piece that was just placed. So only check moves that include
 * the square was just used.
 */
func CheckForWin(x, y, player int, board [][]int) bool {
	var counter int

	/* check for row win */
	for counter = 0; counter < 3; counter++ {
		if board[x][counter] != player {
			break
		}
	}
	if counter == 3 {
		return true
	}

	/* check for column win */
	for counter = 0; counter < 3; counter++ {
		if board[counter][y] != player {
			break
		}
	}
	if counter == 3 {
		return true
	}

	/* check for diagonal win */
	if x == y {
		for counter = 0; counter < 3; counter++ {
			if board[counter][counter] != player {
				break
			}
		}
		if counter == 3 {
			return true
		}
	}
	if x == 2-y {
		for counter = 0; counter < 3; counter++ {
			fmt.Println(board[counter][2-counter])
			if board[counter][2-counter] != player {
				break
			}
		}
		if counter == 3 {
			return true
		}
	}

	return false
}
